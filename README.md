# Secure chat
> An application that respects privacy and takes security seriously

Secure chat is a web application that allows you to communicate in real time with end-to-end (AES-256) encryption. It uses open source software like jQuery, Bootstrap, and CryptoJS and Autolinker.js.

## Requirements
* Linux (optional)
* Node.js
* Apache

## Getting started
* Checkout this project
* Modify client.js socket URL to your needs
* Modify index.html asset URLs (line 12, 85 and 86)
* Enable gzip on Linux (server.js, line 4)
* Disable Apache logs to improve privacy

## License
The MIT License (MIT)

Copyright (c) 2014 Sander de Vos

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.