$(document).ready(function() {
	// Connect to socket
	var socket = io.connect('http://chat.soved.eu:443');

	// Use room ID if available
	if (window.location.hash.length === 0) {
		var room = (Math.random() + 1).toString(36).substring(2);
	} else {
		var room = window.location.hash.substring(1);
	}

	// Show modal
	$('input[id=room]').val(room);
	$('#settings').modal({
		backdrop: 'static',
		keyboard: false
	});

	// Autofocus on room ID
	$('#settings').on('shown.bs.modal', function() {
		$('#room').select();
	});

	// Submit form on button click
	$('button[name=submitSettings]').click(function() {
		$('form[name=settings]').submit();
	});

	$('form[name=settings]').submit(function(event) {
		event.preventDefault();

		// Set room ID and (hashed) password
		room = window.location.hash = $('input[id=room]').val();
		password = CryptoJS.SHA512($('input[id=password]').val() + 'SecureChat').toString();

		// Join room
		socket.emit('subscribe', room);

		// Hide modal and autofocus on message
		$('#settings').modal('hide');
		$('#message').focus();
	});

	// Track window focus
	var focused = true;

	$(window).focus(function() {
		document.title = 'Chat';
		focused = true;
		count = 0;
	});

	$(window).blur(function() {
		focused = false;
	});

	// Append and linkify stripped message
	function newMessage(sender, message) {
		var message = Autolinker.link(message.replace(/<.*?>/g, '').trim());
		$('.messages').append('<li class="list-group-item"><strong>' + sender + ':</strong> ' + message + '</li>');
		$('html, body').scrollTop( $(document).height() ).offset().top;
	}

	socket.on('status', function(update) {
		newMessage('Server', update);
	});

	var count = 0;

	socket.on('message', function(data) {
		// Decrypt and show message
		var decryptedMessage = CryptoJS.AES.decrypt(data.msg, password).toString(CryptoJS.enc.Utf8);
		newMessage(data.sender, decryptedMessage);

		// Show unread message count
		if ( ! focused) {
			count++;

			document.title = 'Chat (' + count + ')';
		}
	});

	$('form[name=message]').submit(function(event) {
		event.preventDefault();

		// Strip tags and trim message
		var message = $('#message').val().replace(/<.*?>/g, '').trim();
		if (message.length > 0 && message.length < 512) {
			// Encrypt and emit message
			var encryptedMessage = CryptoJS.AES.encrypt(message, password).toString();
			socket.emit('message', encryptedMessage);

			newMessage('You', message);

			$('#message').val('').focus();
		}
	});

	socket.on('disconnect', function() {
		newMessage('Server', 'Connection lost...');
	});

	socket.on('reconnect', function() {
		newMessage('Server', 'Connection restored...');
	});
});