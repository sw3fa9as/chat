var io = require('socket.io').listen(443, {
	'browser client minification': true,
	'browser client etag': true,
	'browser client gzip': false // enable on Linux
});

io.set('log level', 1);

io.sockets.on('connection', function(socket) {
	socket.on('subscribe', function(room) {
		socket.join(room);
		socket.set('room', room, function() {
			socket.emit('status', 'Connection established...');
		});

		socket.broadcast.to(room).emit('status', 'User ' + socket.id + ' joined the room...');
	});

	socket.on('message', function(text) {
		text = text.replace(/<.*?>/g, '').trim();
		if (text.length > 0 && text.length < 4096) {
			socket.get('room', function(err, room) {
				socket.broadcast.to(room).emit('message', {
					sender: socket.id,
					msg: text
				});
			});
		}
	});

	socket.on('disconnect', function() {
		socket.get('room', function(err, room) {
			socket.broadcast.to(room).emit('status', 'User ' + socket.id + ' has left the room...');
		});
	});
});